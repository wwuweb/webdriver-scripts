import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import java.util.concurrent.TimeUnit
//System.setProperty("webdriver.chrome.driver", "C:\\ProgramData\\chocolatey\\lib\\chromedriver\\tools\\chromedriver.exe")

def URL = "https://id.siteimprove.com/account/login?returnUrl=%2Fconnect%2Fauthorize%2Flogin%3Fclient_id%3Dmy2%26redirect_uri%3Dhttps%253A%252F%252Fmy2.siteimprove.com%252FAuth%252FAuthCallback%26response_mode%3Dform_post%26response_type%3Dcode%2520id_token%26scope%3Dopenid%2520profile%2520si.profile%26state%3DOpenIdConnect.AuthenticationProperties%253DXrWL690RF0UsRb26WxT7zlsBiEaptpeCmkOLRr9PuatfNVPJYyVfXG56VIUwmmftPond5rnpMnhKTa4Z1i8uTRqtHnlgykjnY5bdqDRTsgUqPtqTl8j_LU89K_Uj8tHQd2y4M46sw3IO_Zzht2HqK9KXKdlqrd0YbsaTLa6y_RR4ngeojWet3r-udaRzDs904BCRYhlc57e2xJHkycFWy_7nydVakJNOE1J_dQ5pCpg%26nonce%3D636846457314111429.ZTA1ZWJlNWEtYWZiZi00ZGQ1LWJiMzctNjdmMzExNzY1YjgyNDY4ZDJlM2QtZDljNC00MDBlLTg2YWYtZDFlNWFiZjNlNjk0%26post_logout_redirect_uri%3Dhttps%253A%252F%252Fmy2.siteimprove.com%252FAuth%252FLogoutCallback";
WebDriver driver = new ChromeDriver()
WebDriverWait wait = new WebDriverWait(driver, 60)
driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)
driver.manage().window().maximize()
driver.navigate().to(URL)

// Get the username textbox and enter username
WebElement loginId = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Email")))
loginId.sendKeys("yourSiteImproveUsername")

// Click the continue button under the username tbox
WebElement continueButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button.btn-primary")))
continueButton.click()

// Set username for SSO and click on 'Next' button
WebElement wwuLoginUsername = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector('input[name="loginfmt"]')))
wwuLoginUsername.sendKeys("yourSiteImproveUsername")
WebElement nextButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector('input[id="idSIButton9"]')))
nextButton.click()

// Set the SSO password and click the 'Sign In' button
WebElement pwordTbox = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector('input[name="passwd"]')))
pwordTbox.sendKeys("yourSiteImprovePassword")
WebElement signInButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector('input[value="Sign in"]')))
signInButton.click()

// Click the annoying 'Yes' button to stay signed in.
WebElement staySignedInButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector('input[id="idSIButton9"]')))
staySignedInButton.click()

// Now that you're at the Siteimprove dashboard, go to the page that lists the issues we're interested in.
driver.get('https://my2.siteimprove.com/Accessibility/468017/20180369320/Pages/Issue?CheckId=133&CriterionChapter=1.3.1')

// Get the links of the issues and do something with em.
def issues = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector('a[class="titleurl"]')))
List<String> hrefs = []
List<WebElement> elements = []
issues.each { issue ->
	hrefs.add(issue.getAttribute("href"))
}

//Loop thru issues and mark the occurrences as Approved...
hrefs.each { href ->
	i = 0
	driver.get(href)
	By container = By.xpath("//div[@class='collapse-group']");
	wait.until(ExpectedConditions.visibilityOfElementLocated((container)))
	def toggles = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//button[@class='is-interactive'][@aria-expanded='false']")))
	def aboutToggle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='is-interactive'][@aria-expanded='true']//div/span[contains(text(),'About')]")))
	if (aboutToggle) {
		processSection("About", driver)
	} else {
	    println("Error:  About accordion never expanded...")
	}
	def academicsToggle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='is-interactive'][@aria-expanded='false']/div/div/div/span[contains(text(),'Academics')]")))
	if (academicsToggle) {
	    academicsToggle.click()   //Expands Academics section
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='is-interactive'][@aria-expanded='true']/div/div[2]/div/span[contains(text(),'Academics')]")))
	} else {
		processSection("Academics", driver)
	}
	toggles[4].click()   //Expands Admissions section
	def admissionsToggle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='is-interactive'][@aria-expanded='true']//div/span[contains(text(),'Admissions')]")))
	processSection("Admissions", driver)
	toggles[5].click()   //Expands Services section
	def studentServicesToggle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='is-interactive'][@aria-expanded='true']//div/span[contains(text(),'Student Services')]")))
	processSection("Student Services", driver)
	toggles[6].click()   //Expands Student Life section
	def studentLifeToggle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='is-interactive'][@aria-expanded='true']//div/span[contains(text(),'Student Life')]")))
	processSection("Student Life", driver)
	toggles[7].click()   //Expands Visitors section
	def visitorsToggle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='is-interactive'][@aria-expanded='true']//div/span[contains(text(),'Visitors')]")))
	processSection("Visitors", driver)
}

WebUI.delay(3)
WebUI.closeBrowser()

def processSection(name, driver) {
	def selector = ""
	def decisionToggle
	def decisionButton
	def approvalButton 
	def applyDecision
	WebDriverWait wait = new WebDriverWait(driver, 60)
	
	//Expand this should per section processing differ depending on name of section.
	switch (name) {
		case ~/^.*$/:
		  selector = "//div[@class='collapse-toggle']/div[@class='collapse']//button[@class='is-interactive button is-outline'][@type='button'][@aria-expanded='false']"
		  decisionToggle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selector)))
		  decisionToggle.click()
		  selector = "//button[@role='radio'][@class='is-interactive'][@tabindex='-1']//span[@class='radio-title'][contains(text(),'Approved')]"
		  approvalButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selector)))
		  approvalButton.click()
		  selector = "//button//span[@class='button-text'][contains(text(),'Apply decision')]"
		  applyDecision = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(selector)))
		  applyDecision[1].click()
		  break
	}
}


